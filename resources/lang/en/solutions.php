<?php
  return [
    'process_level' => [
      'submitted' => 'Solution Submission',
      'pass' => 'Pass',
      'fail' => 'Fail',
      'selected' => 'Selected',
    ]
  ];
 ?>
