<?php
  return [
    'phases' => [
      'problem_submission' => 'Problem Submission'
    ],
    'errors' => [
      'in_active_phase' => "This Phase has been finished"
    ]
  ];
 ?>
