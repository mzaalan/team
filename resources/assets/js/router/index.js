import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '../containers/Full'

// Views
import Register from '../views/register/Register'
import Users from '../views/users/index'
import UserForm from '../views/users/form'
import Login from '../views/auth/Login'
import Teams from '../views/teams'
import TeamForm from '../views/teams/form'
import TeamPlayers from '../views/team_players/index'
import TeamPlayersForm from '../views/team_players/form'


Vue.use(Router)

const router = new Router({
  mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/teams',
      name: 'Home',
      component: Full,
      meta: { requiresAuth: true },
      children: [
        {
          path: 'users',
          redirect: '/',
          name: 'Users',
          component: {
            render (c) { return c('router-view') }
          },
          meta: { requiresAuth: true },
          children: [
            {
              path: '/',
              name: 'All Users',
              component: Users,
              meta: { requiresAuth: true }
            },
            {
              path: 'create',
              name: 'Add New Users',
              component: UserForm,
              meta: { requiresAuth: true }
            }
          ]
        },{
          path: 'teams',
          redirect: '/',
          name: 'Teams',
          component: {
            render (c) { return c('router-view') }
          },
          meta: { requiresAuth: true },
          children: [
            {
              path: '/',
              name: 'All',
              component: Teams,
              meta: { requiresAuth: true }
            },
            {
              path: 'create',
              name: 'New Team',
              component: TeamForm,
              meta: { requiresAuth: true }
            },
            {
              path: ':team_id/players',
              name: 'Team Players',
              component: TeamPlayers,
              meta: { requiresAuth: true }
            },
            {
              path: ':team_id/players/create',
              name: 'New Player',
              component: TeamPlayersForm,
              meta: { requiresAuth: true }
            }
          ]
        }
      ]
    },
    {
      path: '/register',
      redirect: '/',
      name: 'Auth',
      component: {
        render (c) { return c('router-view') }
      },
      meta: { requiresAuth: false },
      children: [
        {
          path: '/',
          name: 'Register',
          component: Register,
          meta: { requiresAuth: false }
        }
      ]
    },
    {
      path: '/login',
      redirect: '/',
      name: 'Login',
      component: {
        render (c) { return c('router-view') }
      },
      meta: { requiresAuth: false },
      children: [
        {
          path: '/',
          name: 'Login',
          component: Login,
          meta: { requiresAuth: false }
        }
      ]
    }

  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let token = localStorage.getItem('access_token');
    if (!token || token == '') {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath,
        },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
