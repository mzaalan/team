// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.use(BootstrapVue/*,axios*/);
Vue.component('pagination', require('laravel-vue-pagination'));
window.axios = require('axios');
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  },created: function () {
    axios.interceptors.request.use((config) => {
      config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('access_token')
      return config;
    }, (error) => {
      if(error.response.status == 401){
        this.$router.push('login')
      }
    });

    axios.interceptors.response.use((response) => {
      return response
    }, (error) => {
      if(error.response.status == 401){
        this.$router.push('/login')
      }
       return Promise.reject(error.response);
    });
  }
})
