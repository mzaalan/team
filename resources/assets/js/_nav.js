export default {
  items: [
    {
      name: 'Users',
      url: '/users',
      icon: 'icon-user',
      children: [
        {
          name: 'Users',
          url: '/users',
          icon: 'icon-user',
        },
        {
          name: 'Add New User',
          url: '/users/create',
          icon: 'icon-plus',
        },
      ]
    },
    {
      name: 'Teams',
      url: '/teams',
      icon: 'icon-star',
      children: [
        {
          name: 'Teams',
          url: '/teams',
          icon: 'icon-star'
        },
        {
          name: 'Add Team',
          url: '/teams/create',
          icon: 'icon-star'
        }
      ]
    }
  ]
}
