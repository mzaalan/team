<?php

use Illuminate\Database\Seeder;
use Team\Models\Team;
use Team\Models\Player;

class InitialDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
        [
          'name' => 'Real Madried',
          'players' => [
            ['first_name' => 'Cristiano', 'last_name' => 'Ronaldo'],
            ['first_name' => 'Sergio', 'last_name' => 'Ramos'],
          ]
        ],
        [
          'name' => 'Barcelona',
          'players' => [
            ['first_name' => 'Leio', 'last_name' => 'Messi'],
            ['first_name' => 'Jordi', 'last_name' => 'Alba'],
          ]
        ]
      ];
      foreach($data as $team):
        $teamObj= Team::create(['name' => $team['name']]);
        foreach($team['players'] as $player):
          $teamObj->players()->create($player);
        endforeach;
      endforeach;
    }
}
