<?php

use Illuminate\Database\Seeder;
use Team\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'name' => 'Admin',
          'email' => 'admin@team.com',
          'password' => bcrypt('adminpass')
        ];
        User::create($data);
    }
}
