<?php

namespace Team\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Team\Repositories\TeamRepository;
use Team\Repositories\UserRepository;
use Team\Repositories\PlayersRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

      app()->bind(
          TeamRepository::class,
          TeamRepository::class
      );

      app()->bind(
        UserRepository::class,
        UserRepository::class
      );

      app()->bind(
        PlayersRepository::class,
        PlayersRepository::class
      );
    }
}
