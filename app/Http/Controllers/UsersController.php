<?php

namespace Team\Http\Controllers;

use Illuminate\Http\Request;
use Team\Http\Requests\CreateRegularUserRequest;
use Team\Jobs\RegisterUserCommand;
use Team\Repositories\UserRepository;

class UsersController extends AppBaseController
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(UserRepository $user_repository)
  {
    $users = $user_repository->getAll();
    return $this->sendResponse($users->toArray(), trans('actions.retrieved'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  Team\Http\Requests\CreateUserRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CreateRegularUserRequest $request)
  {
      $data = $request->all();
      $command = new RegisterUserCommand(
        data_get($data,'name'),
        data_get($data,'email'),
        data_get($data,'password')
      );
      $user = dispatch($command);

      $user = dispatch($command);
      $token = JWTAuth::fromUser($user);

      return $this->sendResponse(array(
          'user' => $user,
          'token' => $token
      ), "Success Login");
  }
}
