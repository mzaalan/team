<?php

namespace Team\Http\Controllers;

use Illuminate\Http\Request;
use Team\Http\Requests\CreateTeamRequest;
use Team\Repositories\TeamRepository;
use Team\Models\Team;
use Team\Models\Player;

class TeamsController extends AppBaseController
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(
    TeamRepository $team_repository
    )
  {
    $limit = config('repository.pagination.limit', 15);
    $teams = $team_repository->paginateAll($limit)->toArray();
    $teams['data'] = collect($teams['data'])->KeyBy('id');
    return $this->sendResponse([
      'teams' => $teams
    ],trans('actions.retrieved'));
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  Team\Http\Requests\CreateTeamRequest  $request
   * @param  Team\Repositories\TeamRepository  $team_repository
   * @return \Illuminate\Http\Response
   */
  public function store(CreateTeamRequest $request, TeamRepository $team_repository)
  {
    $data = ['name' => $request->get('name')];
    $team = $team_repository->create($data);
    if($players = $request->input('members', [])){
      $playersObjects = [];
      foreach ($players as $key => $player) {
        $player['team_id'] = $team->id;
        $playersObjects[$key] = $player;
      }
      Player::insert($playersObjects);
    }
    return $this->sendResponse($team->toArray(), trans('actions.success'));
  }

  public function show(
    Request $request,
    TeamRepository $team_repository,
    $id)
  {
      $team = $team_repository->findWithoutFail($id);
      return $this->sendResponse($team->toArray(), trans('actions.retrieved'));
  }
}
