<?php

namespace Team\Http\Controllers;

use Illuminate\Http\Request;
use Team\Repositories\PlayersRepository;
use Team\Models\Team;
use Team\Models\Player;

class TeamPlayersController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(
      Request $request,
      PlayersRepository $players_repo,
      $team)
    {
        $players = $players_repo->players($team)->toArray();
        $players['data'] = collect($players['data'])->KeyBy('id');
        return $this->sendResponse([
          'players' => $players
        ], trans('actions.retrieved'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(
      Team $team
    )
    {
        return $this->sendResponse(['team' => $team ], trans('actions.retrieved'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PlayersRepository $players_repo,Team $team)
    {
      $data = $request->all();
      $team->players()->create($data);
      return $this->sendResponse([], trans('actions.success'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
