<?php

namespace Team\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

class AuthenticateController extends AppBaseController
{

    public function authenticate(Request $request)
    {
        $this->validateLogin($request);

        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->sendError('Invalid Credentials', 401);
            }
        } catch (JWTException $e) {
            return $this->sendError('An error occurred while trying to make authentication', 500);
        }

        $user = JWTAuth::authenticate($token);

        return $this->sendResponse(array(
            'user' => $user,
            'token' => $token
        ), "Success Login");
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }
}
