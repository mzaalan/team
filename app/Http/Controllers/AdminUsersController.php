<?php

namespace Team\Http\Controllers;

use Illuminate\Http\Request;
use Team\Http\Controllers\Controller;
use Team\Http\Requests\CreateUserRequest;
use Team\Jobs\RegisterUserCommand;
use Team\Models\User;
use Team\Repositories\UserRepository;
use Team\Http\Controllers\AppBaseController;
use JWTAuth;

class AdminUsersController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Team\Http\Requests\CreateUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
      $data = $request->all();
      $command = new RegisterUserCommand(
        data_get($data,'name'),
        data_get($data,'email'),
        data_get($data,'password')
      );
      $user = dispatch($command);

      return $this->sendResponse($user, trans('actions.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
