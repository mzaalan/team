<?php

namespace Team\Http\Controllers;

use Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message,
        ];
        return Response::json($response);
    }

    public function sendError($error, $code = 404, $data = [])
    {
        $res = [
            'success' => false,
            'message' => $error,
            'data' => $data
        ];

        return Response::json($res, $code);
    }

    protected function getUser()
    {
        return JWTAuth::parseToken()->authenticate();
    }
}
