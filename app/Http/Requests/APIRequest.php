<?php

namespace Team\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;

class APIRequest extends FormRequest
{
    /**
     * Get the proper failed validation response for the request.
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $messages = implode(' ', array_flatten($errors));
        $res = [
            'success' => false,
            'message' => $messages,
        ];
        return Response::json($res, 400);
    }
}
