<?php

namespace Team\Repositories;

use Team\Models\Team;

class TeamRepository extends BaseRepository
{
    /**
     * Configure the Model
     **/
    public function model()
    {
        return Team::class;
    }

    public function paginateAll($limit = 15){
      return $this->model->paginate($limit);
    }
}
