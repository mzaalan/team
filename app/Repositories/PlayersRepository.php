<?php

namespace Team\Repositories;

use Team\Models\Player;

class PlayersRepository extends BaseRepository
{
    /**
     * Configure the Model
     **/
    public function model()
    {
        return Player::class;
    }

    public function players($team_id, $limit = null){
      $limit = ($limit) ? $limit : config('repository.pagination.limit', 15);
      $solutions = $this->model->when($team_id,function($q) use ($team_id){
        $q->where('team_id',$team_id);
      })->paginate($limit);
      return $solutions;
    }

}
