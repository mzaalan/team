<?php

namespace Team\Repositories;

use Team\Models\User;

class UserRepository extends BaseRepository
{
    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    public function getAll(){
      return $this->model->get();
    }
}
