<?php

namespace Team\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{


  public $fillable = [
      'name'
  ];

  public $with = [
    'players'
  ];

  public function players(){
    return $this->hasMany(Player::class,'team_id','id');
  }

}
