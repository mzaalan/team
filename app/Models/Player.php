<?php

namespace Team\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    public $fillable = [
        'first_name',
        'last_name',
        'team_id'
    ];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
