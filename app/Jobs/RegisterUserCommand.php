<?php

namespace Team\Jobs;

use Illuminate\Foundation\Bus\Dispatchable;
use Team\Models\User;
use Team\Repositories\UserRepository;

class RegisterUserCommand
{
    use Dispatchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $name;
    private $email;
    private $userRepo;
    private $password;

    public function __construct(
       $name,
       $email,
       $password
      )
    {
      $this->name = $name;
      $this->email = $email;
      $this->password = $password;
      $this->userRepo = app(UserRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      return $this->userRepo->create([
        'name' => $this->name,
        'email' => $this->email,
        'password' => bcrypt($this->password)
      ]);
    }
}
