<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('authenticate', 'AuthenticateController@authenticate');
Route::resource('regular_users', UsersController::class, ['only' => ['store', 'index']]);
Route::group(['middleware' => ['jwt.auth']], function () {
  Route::resource('admin/users', AdminUsersController::class, ['only' => ['store', 'create', 'index']]);
  Route::resource('teams/{team}/players', TeamPlayersController::class, ['only' => ['store', 'index', 'create']]);
  Route::resource('teams', TeamsController::class, ['only' => ['store', 'index','show']]);
});
